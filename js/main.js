$(document).ready(function(){
    if($(window).height() >= 700 && $(window).height() < 90 && $(window).width() > 991){
        $(".hero").css("height", $(window).height() + "px");
        console.log($(window).height());
    }

    $("button.cta[type='button']").click(function(){
        console.log(".modal-content.")
        $(".modal").show();
        if($(".modal-content").outerHeight() > ($(window).height()-40)){
            $("body").css("overflow", "hidden");
        }
        else{
            $(".modal").addClass("modal_flex");
        }
    });

    $(".modal-bg, .modal-close").click(function(){
        $("body").css("overflow", "");
        $(".modal").removeClass("modal_flex");
        $(".modal").hide();
    });

	const heroPhones = document.querySelectorAll(".common-input_phone");
	heroPhones.forEach(function(item){
		IMask(item, {mask: '+{7} (000) 000-00-00'});
	});

    $(".common-form").submit(function(e){
        e.preventDefault();
		$.ajax({
			url: '/form.php',
			method: 'post',
			data: $(this).serialize(),
			success: function(data){
				if(data == "OK"){
					alert("Заявка успешно отправлена. Ожидайте, с Вами свяжутся!");
                    location.reload();
				}
				else{
					alert("Произошла ошибка при отправке формы. Попробуйте позже...");
				}
			}
		});
    });

    $(window).scroll(function(){
        if(($(window).scrollTop()+$(window).height() >= $(".stats-wrapper").offset().top) && $(".stats").attr("data-loaded") != "true"){
            $('.stats-item__title span').spincrement({'duration' : 3500});
            $(".stats").attr("data-loaded", true);

            let blocks = $('.stats-item');
            blocks.each(function(index, obj) {
                var block = $(obj);
                setTimeout(function() {
                    block.addClass('stats-item_visible');
                }, 200*index);
            });
        }

        if(($(window).scrollTop()+$(window).height() >= $(".employees-stats").offset().top) && $(".employees").attr("data-loaded") != "true"){
            $('.employee-item__title span').spincrement({'duration' : 2000});
            $(".employees").attr("data-loaded", true);

            let blocks = $('.employee-item');
            blocks.each(function(index, obj) {
                var block = $(obj);
                setTimeout(function() {
                    block.addClass('employee-item_visible');
                }, 200*index);
            });
        }
    });
});